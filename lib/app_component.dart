// Copyright (c) 2016, <your name>. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'package:angular2/core.dart';
import 'filters_component.dart';
import 'users_component.dart';

@Component(selector: 'my-app',
    templateUrl: 'app_component.html',
    directives: const [UsersComponent, FiltersComponent])
class AppComponent {
  var users = new List();
  var filters = new List();
}
