import 'package:angular2/core.dart';
import 'user.dart';
import 'filter.dart';
@Component(selector: 'filters',
    templateUrl: 'filters_component.html'
)

class FiltersComponent  {

  @Input() set users(value) {
    this._users = value;
    print('users set');
    if(users.length > 0) {
      print('filters inited');
      initFilters();
    }
  }
  List<User> get users {
    return this._users;
  }
  List<User> _users = new List<User>();

  @Output() EventEmitter<List<Filter>> filtersChanged = new EventEmitter<List<Filter>>();

  List<Filter> filters = new List<Filter>();

  void updateFilters()
  {
     var visibleUsers = users.where((u) => u.visible(filters));
     for(int i = 0; i < filters.length; i++)
     {
       for(int j = 0; j < filters[i].options.length; j++)
       {
         int newCount = visibleUsers.where((u) => u.getProperty(filters[i].type) == filters[i].options[j].name).length;
         print('new count: ' + newCount.toString());
         filters[i].options[j].count = newCount;
       }
     }
  }



  void initFilters()
  {
    filters.clear();

    var departmentFilter = new Filter.fromUsers(FilterType.department, users);
    filters.add(departmentFilter);

    var genderFilter = new Filter.fromUsers(FilterType.gender, users);
    filters.add(genderFilter);

    var cityFilter = new Filter.fromUsers(FilterType.city, users);
    filters.add(cityFilter);

    for(int i = 0; i < filters.length; i++)
    {
      filters[i].filterChanged.listen((message){
        print('filtersChanged');
        updateFilters();
        filtersChanged.emit(filters);});
    }
  }

  }


