import 'dart:html';
import 'user.dart';
import 'dart:async';
import 'dart:convert';
import 'package:angular2/core.dart';

@Injectable()
class UserService {

  String _url =  "http://putsreq.com/jM5furHtCl8088VKZPzt";

  static List<User> users;

  Future<List<User>> getUsers() async {
    if(users == null) {
      users = await _loadData();
      return users;
    }
    else
    {
      return new Future.value(users);
    }
  }

  Future<List<User>> _loadData() async {

    String response = await HttpRequest.getString(_url);

    List<Map> jsonList = JSON.decode(response);

    var res = _parseData(jsonList);

    return res;
  }

  List<User> _parseData(List<Map> jsonList){

    List<User> result = new List<User>();

    for(int i = 0; i < jsonList.length; i++)
    {
      var u = new User.fromJson(jsonList[i]);
      result.add(u);
    }

    return result;
  }



}