class Address
{
  String city;
  String street;
  Address.fromJson(Map json) {
    city = json['city'];
    street = json['street'];
  }
  compareTo(Address another)
  {
    return (this.city + this.street).compareTo(another.city + another.street);
  }
}