import 'filter_option.dart';
import 'dart:async';
import 'user.dart';

enum FilterType {
  department, gender, city
}

class Filter
{
  List<FilterOption> options = new List<FilterOption>();

  FilterOption selected;

  onSelect(FilterOption o)
  {
    this.selected = this.selected != o ? o : null;
    notifySubscribers();
  }

  notifySubscribers() async
  {
    await _filterChangedController.add("filter changed");
  }

  StreamController _filterChangedController = new StreamController.broadcast();
  Stream get filterChanged => _filterChangedController.stream;

  FilterType type;
  String get name =>  type.toString().substring(type.toString().indexOf('.') + 1); //enum to string

  Filter(this.type, List<Object> list)
  {
    if(list.length < 1)
      return;

    FilterOption prev = new FilterOption(list[0]);
    for(int i = 1; i < list.length; i++)
    {
      if(prev.name == list[i])
      {
        prev.count++;
      }
      else
      {
        options.add(prev);
        prev = new FilterOption(list[i]);
      }
    }
    options.add(prev);

  }
  factory Filter.fromUsers(type, List<User> users) {
    var propertyValues = users.map((u) =>  u.getProperty(type)).toList();
    propertyValues.sort();
    var result = new Filter(type, propertyValues);
    return result;
  }
}