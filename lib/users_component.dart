import 'package:angular2/core.dart';
import 'user.dart';
import 'filter.dart';
import 'users_service.dart';

@Component(selector: 'users',
    templateUrl: 'users_component.html',
    providers: const [
      UserService
    ])
class UsersComponent  implements OnInit  {

  final UserService _userService;
  UsersComponent(this._userService);

  String _sortType = 'name'; // set the default sort type
  set sortType(String value) {
    this._sortType = value;
    updateUsersSort();
  }
  String get sortType {
    return this._sortType;
  }

  void updateUsersSort()
  {
    print('users sorted');

    users.sort((User a, User b) {
      print(a.getPropertyByString(sortType));
      int res = a.getPropertyByString(sortType).compareTo(b.getPropertyByString(sortType));
      return sortReverse? -1*res: res;
    });
  }

  bool sortReverse = false;  // set the default sort order

  ngOnInit() {
    getUsers();
  }

  getUsers() async {
    users = await _userService.getUsers();
    updateUsersSort();
    sortReverse = !sortReverse;
    usersChanged.emit(users);
   }

  List<User> users = new List<User>();

  @Input() List<Filter> filters;

  @Output() EventEmitter<List<User>> usersChanged = new EventEmitter<List<User>>();

}
