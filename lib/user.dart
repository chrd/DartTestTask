import 'address.dart';
import 'filter.dart';
class User
{
  int id;
  String name;
  int age;
  String gender;
  String department;
  Address address;
  User(this.name, this.age, this.gender, this.department);
  User.fromJson(Map json) {
    id = json['id'];
    name = json['name'];
    age = json['age'];
    gender = json['gender'];
    department = json['department'];
    address = new Address.fromJson(json['address']);
  }

  bool visible(List<Filter> filters)
  {
    for(int i = 0; i < filters.length; i++)
    {
      if(filters[i].selected == null)
        continue;

      var selected = filters[i].selected.name;

      if(selected != getProperty(filters[i].type))
        return false;
    }

    return true;
  }

  getProperty(FilterType type)
  {
    switch(type)
    {
      case FilterType.department:
        return this.department;
      case FilterType.gender:
        return this.gender;
      case FilterType.city:
        return this.address.city;
    }
  }
  getPropertyByString(String prop)
  {
    switch(prop)
    {
      case 'age':
        return this.age;
      case 'name':
        return this.name;
      case 'department':
        return this.department;
      case 'gender':
        return this.gender;
      case 'address':
        return this.address;
      case 'city':
        return this.address.city;
      case 'street':
        return this.address.street;

    }
  }
}